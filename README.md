# nopanick-rs (No Panic!)

No panic library for add, sub, mul, div operators in Rust.

## example
```Rust
use nopanick::*;

fn main() {
    unsafe {
        println!("{}", div!(sub!(add!(100i32, 10i32), 5i32), 5i32));
    }
}
```

## Building

Release only for optimization effect:

```shell
cargo build --release
```


### Contact me
- Web: https://github.com/hwoy 
- Email: zaintcorp@yahoo.com 
- Facebook: http://www.facebook.com/dead-root
