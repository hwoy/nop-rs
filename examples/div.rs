use nopanick::*;

fn main() {
    unsafe {
        println!("{}", div!(sub!(add!(100i32, 10i32), 5i32), 5i32));
    }
}
