use nopanick::*;

fn rem(a: u32, b: u32) -> Option<u32> {
    if b != 0 {
        Some(a % b)
    } else {
        None
    }
}

fn main() {
    let a = 4u32;
    let b = 3u32;
    let r = unsafe { rem(a, b).unwrap_nop() };

    println!("{} % {} ={}", a, b, r);
}
