//! # nopanick-rs (No Panic!)
//!
//! No panic library for add, sub, mul, div operators in Rust.
//!
//! Generates less instructions, suite for barematal or embedded system.
//!
//! ## Example#1
//! ```
//! use nopanick::*;
//!
//! fn main() {
//!     unsafe {
//!         println!("{}", div!(sub!(add!(100i32, 10i32), 5i32), 5i32));
//!     }
//! }
//! ```
//! ## Example#2
//! How to use ```.unwarp_nop()``` ```with Option<T>```
//! ```
//! use nopanick::*;
//!
//! fn rem(a: u32, b: u32) -> Option<u32> {
//!     if b != 0 {
//!         Some(a % b)
//!     } else {
//!         None
//!     }
//! }
//!
//! fn main() {
//!     let a = 4u32;
//!     let b = 3u32;
//!     let r = unsafe { rem(a, b).unwrap_nop() };
//!
//!     println!("{} % {} ={}", a, b, r);
//! }
//!
//! ```
//!
//! ## Example#3
//! If ```None``` was unwraped, UB occurs.
//! ```
//! use nopanick::*;
//!
//! fn rem(a: u32, b: u32) -> Option<u32> {
//!     if b != 0 {
//!         Some(a % b)
//!     } else {
//!         None
//!     }
//! }
//!
//! fn main() {
//!     let a = 4u32;
//!     let b = 0u32;
//!     let r = unsafe { rem(a, b).unwrap_nop() };
//!
//!     println!("{} % {} ={}", a, b, r);
//! }
//! //error: process didn't exit successfully: `target\debug\examples\option.exe` (exit code: 0xc000001d, STATUS_ILLEGAL_INSTRUCTION) Illegal instruction
//! ```

#![allow(unused)]
#![no_std]

extern crate core;
use core::hint;

/// Function like .unwrap_unchecked() for ```Option<T>```
#[inline(always)]
pub unsafe fn unwrap_option<T>(option: Option<T>) -> T {
    match option {
        Some(val) => val,
        _ => hint::unreachable_unchecked(),
    }
}

/// Function like .unwrap_unchecked() for ```Result<T,E>```
#[inline(always)]
pub unsafe fn unwrap_result<T, E>(result: Result<T, E>) -> T {
    match result {
        Ok(val) => val,
        _ => hint::unreachable_unchecked(),
    }
}

/// This trait provides ```unsafe fn unwrap_nop(self) -> Self::Output ``` for ```Option<T>``` and ```Result<T,E>```
pub unsafe trait Nop {
    type Output;
    /// Like ```unwrap_unchecked()``` but can generate less instructtion code.
    ///
    ///  # Use
    ///  ```
    /// use nopanick::Nop;
    /// fn div(a:i32,b:i32)->i32
    /// {
    ///     unsafe{ a.checked_div(b).unwrap_nop() }
    /// }
    ///  ```
    unsafe fn unwrap_nop(self) -> Self::Output;
}

unsafe impl<T> Nop for Option<T> {
    type Output = T;

    #[inline(always)]
    unsafe fn unwrap_nop(self) -> <Self as Nop>::Output {
        unwrap_option(self)
    }
}

unsafe impl<T, E> Nop for Result<T, E> {
    type Output = T;

    #[inline(always)]
    unsafe fn unwrap_nop(self) -> <Self as Nop>::Output {
        unwrap_result(self)
    }
}

macro_rules! assign2 {
    ($i:ident,$j:ident) => {
        #[macro_export]
        macro_rules! $i {
            ($a:expr,$b:expr) => {{
                unwrap_option(($a).$j($b))
            }};
        }
    };
}

macro_rules! assign1 {
    ($i:ident,$j:ident) => {
        #[macro_export]
        macro_rules! $i {
            ($a:expr) => {{
                unwrap_option(($a).$j())
            }};
        }
    };
}

/// Macro for unpanic add.
///
/// compile with optimization for non panicking and may produce UB.
///
/// # Use
/// ```
/// use nopanick::unwrap;
/// let a=unsafe{ nopanick::add!(1i32,0i32) };
///  ```
///  use macro add with ```unsafe``` because of ```std::hint::unreachable_checked()```
assign2!(add, checked_add);

/// Macro for unpanic sub.
///
/// compile with optimization for non panicking and may produce UB.
///
/// # Use
/// ```
/// use nopanick::unwrap;
/// let a=unsafe{ nopanick::sub!(1i32,0i32) };
///  ```
///  use macro sub with ```unsafe``` because of ```std::hint::unreachable_checked()```
assign2!(sub, checked_sub);

/// Macro for unpanic mul.
///
/// compile with optimization for non panicking and may produce UB.
///
/// # Use
/// ```
/// use nopanick::unwrap;
/// let a=unsafe{ nopanick::mul!(1i32,0i32) };
///  ```
///  use macro add with ```unsafe``` because of ```std::hint::unreachable_checked()```
assign2!(mul, checked_mul);

/// Macro for unpanic div.
///
/// compile with optimization for non panicking and may produce UB.
///
/// # Use
/// ```
/// use nopanick::unwrap;
/// let a=unsafe{ nopanick::div!(1i32,1i32) };
///  ```
///  use macro div with ```unsafe``` because of ```std::hint::unreachable_checked()```
assign2!(div, checked_div);

/// Macro for unpanic rem.
///
/// compile with optimization for non panicking and may produce UB.
///
/// # Use
/// ```
/// use nopanick::unwrap;
/// let a=unsafe{ nopanick::rem!(1i32,2i32) };
///  ```
///  use macro rem with ```unsafe``` because of ```std::hint::unreachable_checked()```
assign2!(rem, checked_rem);

/// Macro for unpanic shr.
///
/// compile with optimization for non panicking and may produce UB.
///
/// # Use
/// ```
/// use nopanick::unwrap;
/// let a=unsafe{ nopanick::shr!(1i32,0u32) };
///  ```
///  use macro shr with ```unsafe``` because of ```std::hint::unreachable_checked()```
assign2!(shr, checked_shr);

/// Macro for unpanic shl.
///
/// compile with optimization for non panicking and may produce UB.
///
/// # Use
/// ```
/// use nopanick::unwrap;
/// let a=unsafe{ nopanick::shl!(1i32,0u32) };
///  ```
///  use macro shl with ```unsafe``` because of ```std::hint::unreachable_checked()```
assign2!(shl, checked_shl);

/// Macro for unpanic neg.
///
/// compile with optimization for non panicking and may produce UB.
///
/// # Use
/// ```
/// use nopanick::unwrap;
/// let a=unsafe{ nopanick::neg!(1i32) };
///  ```
///  use macro neg with ```unsafe``` because of ```std::hint::unreachable_checked()```
assign1!(neg, checked_neg);

/// Macro for unpanic abs.
///
/// compile with optimization for non panicking and may produce UB.
///
/// # Use
/// ```
/// use nopanick::unwrap;
/// let a=unsafe{ nopanick::abs!(1i32) };
///  ```
///  use macro abs with ```unsafe``` because of ```std::hint::unreachable_checked()```
assign1!(abs, checked_abs);

/// Macro for unpanic pow.
///
/// compile with optimization for non panicking and may produce UB.
///
/// # Use
/// ```
/// use nopanick::unwrap;
/// let a=unsafe{ nopanick::pow!(1i32,0u32) };
///  ```
///  use macro pow with ```unsafe``` because of ```std::hint::unreachable_checked()```
assign2!(pow, checked_pow);

assign2!(ilog, checked_ilog);

assign1!(ilog10, checked_ilog10);

assign1!(ilog2, checked_ilog2);

assign2!(div_euclid, checked_div_euclid);

assign2!(rem_euclid, checked_rem_euclid);

assign2!(add_unsigned, checked_add_unsigned);

assign2!(sub_unsigned, checked_sub_unsigned);
