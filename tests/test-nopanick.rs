extern crate nopanick;

#[cfg(test)]
mod tests {

    use nopanick::*;

    #[test]
    fn test_basic() {
        assert_eq!(0 + 0, 0);
        assert_eq!(1 + 0, 1);
        assert_eq!(0 + 1, 1);
        assert_eq!(0 + 1, 1 + 0);

        assert_eq!(0 - 0, 0);
        assert_eq!(1 - 0, 1);
        assert_eq!(0 - 1, -1);

        assert_eq!(1 * 0, 0);
        assert_eq!(0 * 1, 0);
        assert_eq!(1 * 0, 0 * 1);
        assert_eq!(1 * 1, 1);

        assert_eq!(1 / 1, 1);
        assert_eq!(1 / 2, 0);
        assert_eq!(1 / 3, 0);

        assert_eq!(1 % 1, 0);
        assert_eq!(1 % 2, 1);
        assert_eq!(1 % 3, 1);
    }

    #[test]
    fn test_option() {
        assert_eq!(1u32.checked_div(0u32), None);
        assert_eq!(10i32.checked_div(3i32), Some(10i32 / 3i32));

        assert_eq!(
            unsafe { 10i32.checked_div(3i32).unwrap_nop() },
            (10i32 / 3i32)
        );
    }

    #[test]
    fn test_result() {
        let rok: Result<i32, &str> = Ok(2);
        let rerr: Result<i32, &str> = Err("Error");

        assert_eq!(rok, Ok(2));
        assert_eq!(rerr, Err("Error"));
        assert_eq!(unsafe { rok.unwrap_nop() }, 2);
    }

    #[test]
    fn test_add() {
        let a: i32 = -100;
        let b: i32 = 2;
        assert_eq!(unsafe { add!(a, b) }, a + b);
    }

    #[test]
    fn test_sub() {
        let a: i32 = -100;
        let b: i32 = -2;
        assert_eq!(unsafe { sub!(a, b) }, a - b);
    }

    #[test]
    fn test_mul() {
        let a: i32 = -100;
        let b: i32 = -2;
        assert_eq!(unsafe { mul!(100i32, 2i32) }, a * b);
        assert_eq!(unsafe { mul!(a, b) }, a * b);
    }

    #[test]
    fn test_div() {
        let a: i32 = -100;
        let b: i32 = -2;
        assert_eq!(unsafe { div!(a, b) }, a / b);
    }

    #[test]
    fn test_rem() {
        let a: i32 = -100;
        let b: i32 = -2;
        assert_eq!(unsafe { rem!(a, b) }, a % b);
    }

    #[test]
    fn test_div_trait() {
        let a: i32 = -100;
        let b: i32 = -2;
        assert_eq!(unsafe { a.checked_div(b).unwrap_nop() }, a / b);
    }

    #[test]
    fn test_abs() {
        let a: i32 = -100;
        let b: i32 = -2;
        assert_eq!(unsafe { abs!(b - a) }, i32::abs(b - a));
    }

    #[test]
    fn test_neg() {
        let a: i32 = -100;
        let b: i32 = -2;
        assert_eq!(unsafe { neg!(b - a) }, -(b - a));
    }

    #[test]
    fn test_ilog() {
        assert_eq!(unsafe { ilog!(5i32, 5) }, 5i32.ilog(5));
    }

    #[test]
    fn test_ilog10() {
        assert_eq!(unsafe { ilog10!(10i32) }, 10i32.ilog10());
    }

    #[test]
    fn test_ilog2() {
        assert_eq!(unsafe { ilog2!(2i32) }, 2i32.ilog2());
    }

    #[test]
    fn test_div_euclid() {
        assert_eq!(unsafe { div_euclid!(-7i32, 4) }, (-7i32).div_euclid(4));
    }

    #[test]
    fn test_rem_euclid() {
        assert_eq!(unsafe { rem_euclid!(-7i32, 4) }, (-7i32).rem_euclid(4));
    }

    #[test]
    fn test_add_euclid() {
        assert_eq!(
            unsafe { add_unsigned!(-7i32, 4) },
            (-7i32).checked_add_unsigned(4).unwrap()
        );
        assert_eq!(
            unsafe { -add_unsigned!(7i32, 4) },
            -7i32.checked_add_unsigned(4).unwrap()
        );
    }

    #[test]
    fn test_sub_euclid() {
        assert_eq!(
            unsafe { sub_unsigned!(-7i32, 4) },
            (-7i32).checked_sub_unsigned(4).unwrap()
        );
    }
}
